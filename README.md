## DOCUMENTATION ##

All of the addresses start with https://localhost:5001

WebAPI is working with DB from Docker dotnet_gryczka in dotnet_docker folder. If there will be any problem with docker, you can download PostgreSQL 10 and run migration from file init.sql manually. 

#### List of transactions #### 
<B> User </B>
* Get list of all users 
    <B> GET /User </B>
    Response:
    ```
    [
        {
            "id":  "132545d5-49d9-427f-83a7-e5622b648108",
            "name": "Magda",
            "roles": [
                "Tester"
            ]
        },
        {
            "id": "cdcb3266-fa78-4def-b70d-49f4b204449e",
            "name": "Martyna",
            "roles": []
        }
    ]
    ```
    
    | Field name |                              Description                              |
    |------------|-----------------------------------------------------------------------|
    | Id         | Id of the user (mandatory)                                            |
    | Name       | Name of the user (mandatory)                                          |
    | Roles      | Vector of role names (if empty then user is not assigned to any role) |
* Get one user
    <B> GET /User/\<UserId\> </B> 
    np. /User/132545d5-49d9-427f-83a7-e5622b648108
    Response:
    ```
    {
        "id": "132545d5-49d9-427f-83a7-e5622b648108",
        "name": "Magda",
        "roles": [
            "Tester"
        ]
    }
    ```
    | Field name |                              Description                              |
    |------------|-----------------------------------------------------------------------|
    | Id         | Id of the user (mandatory)                                            |
    | Name       | Name of the user (mandatory)                                          |
    | Roles      | Vector of role names (if empty then user is not assigned to any role) |
* Insert one user
    <B> POST /User </B> 
    Body:
    ```
    {
        "name": "Benny",
        "roleIds": [
            "f04d09ea-01b4-4ee4-889a-25d296f05eda"
        ]
    }
    ```
    Response:
    ```
    {
        "id": "680f591f-1ad8-495f-98f4-27158d80297e"
    }
    ```
    | Field name |                              Description                              |
    |------------|-----------------------------------------------------------------------|
    | Name       | Name of the user (mandatory)                                          |
    | RoleIds    | Vector of role ids (if empty then user is not assigned to any role) |
* Update one user
    <B> PUT /User/\<UserId\> </B> 
    Np. /User/680f591f-1ad8-495f-98f4-27158d80297e
    Body:
    ```
    {
        "name": "Benny",
        "roleIds": []
    }
    ```
    Response:
    ```
    {
        "id": "680f591f-1ad8-495f-98f4-27158d80297e"
    }
    ```
    | Field name |                              Description                              |
    |------------|-----------------------------------------------------------------------|
    | Name       | Name of the user (mandatory)                                          |
    | RoleIds    | Vector of role ids (if empty then user is not assigned to any role) |
* Delete one user
    <B> DELETE /User/\<UserId\> </B> 
    Np. /User/680f591f-1ad8-495f-98f4-27158d80297e
    Response:
    ```
    {
        "id": "680f591f-1ad8-495f-98f4-27158d80297e"
    }
    ```
<B> Role </B>
* Get list of all roles
    <B> GET /Role </B> 
    Response:
    ```
    [
        {
            "id": "f04d09ea-01b4-4ee4-889a-25d296f05eda",
            "name": "Tester",
            "users": [
                "Magda"
            ]
        },
        {
            "id": "a409d363-83aa-41ef-a021-f2476574b38e",
            "name": "Developer",
            "users": []
        }
    ]
    ```
    | Field name |                              Description                              |
    |------------|-----------------------------------------------------------------------|
    | Id         | Id of the role (mandatory)                                            |
    | Name       | Name of the role (mandatory)                                          |
    | Users      | Vector of user names (if empty then role is not assigned to any user) |
* Get one role
    <B> GET /Role/\<RoleId\> </B> 
    np. /Role/f04d09ea-01b4-4ee4-889a-25d296f05eda
    Response: 
    ```
    {
        "id": "f04d09ea-01b4-4ee4-889a-25d296f05eda",
        "name": "Tester 2",
        "users": [
            "Madzia"
        ]
    }
    ```
    | Field name |                              Description                              |
    |------------|-----------------------------------------------------------------------|
    | Id         | Id of the role (mandatory)                                            |
    | Name       | Name of the role (mandatory)                                          |
    | Users      | Vector of user names (if empty then role is not assigned to any user) |
* Insert one role
    <B> POST /Role</B> 
    Body:
    ```
    {
        "name": "Developer",
        "userIds": [
            "680f591f-1ad8-495f-98f4-27158d80297e"
        ]
    }
    ```
    Response:
    ```
    {
        "id": "a409d363-83aa-41ef-a021-f2476574b38e"
    }
    ```
    | Field name |                              Description                              |
    |------------|-----------------------------------------------------------------------|
    | Name       | Name of the role (mandatory)                                          |
    | UserIds    | Vector of user ids (if empty then role is not assigned to any user) |
* Update one role
    <B> UT /Role/\<RoleId\></B> 
    np. /Role/a409d363-83aa-41ef-a021-f2476574b38e
    Body:
    ```
    {
        "name": "Developer",
        "userIds": []
    }
    ```
    Response:
    ```
    {
        "id": "a409d363-83aa-41ef-a021-f2476574b38e"
    }
    ```
    | Field name |                              Description                              |
    |------------|-----------------------------------------------------------------------|
    | Name       | Name of the role (mandatory)                                          |
    | UserIds    | Vector of user ids (if empty then role is not assigned to any user) |
* Delete one role
    <B> DELETE /Role/\<RoleId\></B> 
    np. /Role/a409d363-83aa-41ef-a021-f2476574b38e
    Response: 
    ```
    {
        "id": "a409d363-83aa-41ef-a021-f2476574b38e"
    }