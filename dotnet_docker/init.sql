CREATE DATABASE dotnet_prod;
\c dotnet_prod

CREATE TABLE user_tab (
    user_id UUID PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE role (
    role_id UUID PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE user_role (
    user_id UUID REFERENCES user_tab (user_id),
    role_id UUID REFERENCES role (role_id),
    CONSTRAINT user_role_pkey PRIMARY KEY (user_id, role_id)
);

CREATE ROLE production WITH LOGIN PASSWORD 'production';
GRANT DELETE, SELECT, INSERT, UPDATE ON user_tab, role, user_role TO production;


CREATE DATABASE dotnet_test;
\c dotnet_test

CREATE TABLE user_tab (
    user_id UUID PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE role (
    role_id UUID PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE user_role (
    user_id UUID REFERENCES user_tab (user_id),
    role_id UUID REFERENCES role (role_id),
    CONSTRAINT user_role_pkey PRIMARY KEY (user_id, role_id)
);

CREATE ROLE test WITH LOGIN PASSWORD 'test';
GRANT DELETE, SELECT, INSERT, UPDATE ON user_tab, role, user_role TO test;
