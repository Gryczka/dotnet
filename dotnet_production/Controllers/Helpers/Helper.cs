using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using dotnet_production.Models.Entity;
using dotnet_production.Models.Object;
using Microsoft.EntityFrameworkCore;

namespace dotnet_production.Controllers.Helpers
{
    public class Helper
    {
        private readonly dotnet_prodContext _context;
        public Helper(dotnet_prodContext context) {
            _context = context;
        }

        public List<string> listOfNameRolesForRoleId(Guid roleId) {
            List<string> users = new List<string>();
            var userRoles = _context.UserRole.Where(ur => ur.RoleId == roleId).ToList();
            foreach (var userRole in userRoles) {
                var user = _context.UserTab.Single(u => u.UserId == userRole.UserId);
                if (user != null) {
                    users.Add(user.Name);
                }
            }
            return users;
        }

        public List<string> listOfNameRolesForUserId(Guid userId) {
            List<string> roles = new List<string>();
            var userRoles = _context.UserRole.Where(ur => ur.UserId == userId).ToList();
            foreach (var userRole in userRoles) {
                var role = _context.Role.Single(r => r.RoleId == userRole.RoleId);
                if (role != null) {
                    roles.Add(role.Name);
                }
            }
            return roles;
        }

        public async Task removesUserRolesWithRuleId(Guid ruleId) {
            List<UserRole> userRolesToRemove = await _context.UserRole.Where(ur => ur.RoleId == ruleId).ToListAsync();
            removesUserRoles(userRolesToRemove);
        }

        public async Task removesUserRolesWithUserId(Guid userId) {
            List<UserRole> userRolesToRemove = await _context.UserRole.Where(ur => ur.UserId == userId).ToListAsync();
            removesUserRoles(userRolesToRemove);
        }

        public void removesUserRoles(List<UserRole> userRolesToRemove) {
            if (userRolesToRemove != null && userRolesToRemove.Count() != 0) {
                foreach (var userRoleToRemove in userRolesToRemove) {
                    _context.Remove(userRoleToRemove);
                }
            } 
        }

        public async Task<ResultObj<UserRole>> collectUserRolesForUserIdsFromRole(RoleObj role, Guid roleId) {
            List<UserRole> userRoles = new List<UserRole>();
            foreach (var userId in role.UserIds) {
                var user = await _context.UserTab.FirstAsync(u => u.UserId == userId);
                if (user == null) {
                    return new ResultObj<UserRole>(Result.NotFound, "Not found user with id '" + userId + 
                        "' which is connected to role '" + roleId + "'");
                }
                UserRole userRole = new UserRole { UserId = userId, RoleId = roleId };
                userRoles.Add(userRole);
            }
            return new ResultObj<UserRole>(Result.Ok, userRoles);
        }

        public async Task<ResultObj<UserRole>> collectUserRolesForRoleIdsFromUser(UserObj user, Guid userId) {
            List<UserRole> userRoles = new List<UserRole>();
            foreach (var roleId in user.RoleIds) {
                var role = await _context.Role.FirstAsync(r => r.RoleId == roleId);
                if (role == null) {
                    return new ResultObj<UserRole>(Result.NotFound, "Not found role with id '" + roleId + 
                        "' which is connected to user '" + userId + "'");
                }
                UserRole userRole = new UserRole { UserId = userId, RoleId = roleId };
                userRoles.Add(userRole);
            }
            return new ResultObj<UserRole>(Result.Ok, userRoles);
        }
    }
}
