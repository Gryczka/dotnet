using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using dotnet_production.Models.Entity;
using dotnet_production.Models.Object;
using Microsoft.EntityFrameworkCore;

namespace dotnet_production.Controllers.Helpers
{
    public enum Result {
        Ok,
        NotFound
    }

    public class ResultObj<T> {
        public Result _result;
        public List<T> _list;
        public String _message;

        public ResultObj(Result result, List<T> list) {
            _result = result;
            _list = list;
        }

        public ResultObj(Result result, String message) {
            _result = result;
            _message = message;
        }
    }
}
