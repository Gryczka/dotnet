using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using dotnet_production.Models.Entity;
using dotnet_production.Models.Object;
using dotnet_production.Controllers.Helpers;

namespace dotnet_production.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RoleController : ControllerBase
    {
        private readonly dotnet_prodContext _context;
        private Helper _helper;

        public RoleController(dotnet_prodContext context)
        {
            _context = context;
            _helper = new Helper(context);
        }

        [HttpGet]
        public async Task<IActionResult> GetRoles()
        {
            try
            {
                List<Role> roles = await _context.Role.ToListAsync();

                var listOfRoles = roles.Select(role => {
                    List<string> users = _helper.listOfNameRolesForRoleId(role.RoleId);
                    return new RoleToReturn{
                        Id = role.RoleId,
                        Name = role.Name,
                        Users = users
                    };
                }).ToList();
                if (listOfRoles == null) {
                    return NotFound(new { status = "Roles do not exist" });
                } else {
                    return Ok(listOfRoles);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new { status = "There was an exception: " + ex });
            }
        }

        [HttpGet("{RoleID}")]
        public async Task<IActionResult> GetRole(Guid RoleID)
        {
            try
            {
                Role role = await _context.Role.SingleOrDefaultAsync(r => r.RoleId == RoleID);
                if (role == null) {
                    return NotFound(new { status = "There is no role to return with id '" + RoleID + "'" });
                }
                List<string> users = _helper.listOfNameRolesForRoleId(role.RoleId);
                return Ok(new RoleToReturn{
                    Id = role.RoleId,
                    Name = role.Name,
                    Users = users
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new { status = "There was an exception: " + ex });
            }
        }

        [HttpPost]
        public async Task<IActionResult> PostRole([FromBody] RoleObj role)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (role.Name == null) {
                        return BadRequest(new { status = "Role name has to exist in json" });
                    }
                    if (role.UserIds == null) {
                        return BadRequest(new { status = "Role user ids have to exist in json" });
                    }
                    Guid roleId = Guid.NewGuid();
                    var result = await _helper.collectUserRolesForUserIdsFromRole(role, roleId);
                    if (result._result == Result.NotFound) {
                        return NotFound( new { status = result._message});
                    }
                    List<UserRole> userRoles = result._list;

                    _context.Role.Add(new Role(role, roleId, userRoles));
                    await _context.SaveChangesAsync();
                    return Ok(new GuidToReturn{ Id = roleId});
                }
                else
                {
                    return BadRequest(new { status = "Model is invalid" });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new { status = "There was an exception: " + ex });
            }
        }

        [HttpPut("{RoleID}")]
        public async Task<IActionResult> PutRole([FromRoute] Guid RoleID, [FromBody] RoleObj role) {
            try
            {
                if (ModelState.IsValid)
                {
                    if (role.Name == null) {
                        return BadRequest(new { status = "Role name has to exist in json" });
                    }
                    if (role.UserIds == null) {
                        return BadRequest(new { status = "Role user ids have to exist in json" });
                    }
                    Role roleToRemove = await _context.Role.SingleOrDefaultAsync(r => r.RoleId == RoleID);
                    if (roleToRemove == null) return NotFound (new { status = "Not found role with id '" + RoleID + "'" });
                    await _helper.removesUserRolesWithRuleId(RoleID);
                    var result = await _helper.collectUserRolesForUserIdsFromRole(role, RoleID);
                    if (result._result == Result.NotFound) {
                        return NotFound( new { status = result._message});
                    }
                    List<UserRole> userRolesToAdd = result._list;
                    if (roleToRemove != null)  {
                        _context.Remove(roleToRemove);
                    }

                    _context.Role.Add(new Role(role, RoleID, userRolesToAdd));
                    await _context.SaveChangesAsync();
                    return Ok(new GuidToReturn{ Id = RoleID});
                }
                else
                {
                    return BadRequest(new { status = "Model is Invalid" });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new { status = "There was an exception: " + ex });
            }
        }

        [HttpDelete("{RoleID}")]
        public async Task<IActionResult> DeleteRole(Guid RoleID)
        {
            try
            {
                Role roleToRemove = await _context.Role.SingleOrDefaultAsync(r => r.RoleId == RoleID);
                await _helper.removesUserRolesWithRuleId(RoleID);
                if (roleToRemove != null)  {
                    _context.Remove(roleToRemove);
                    await _context.SaveChangesAsync();
                    return Ok(new GuidToReturn{ Id = RoleID});
                } else {
                    return NotFound(new { status = "There is no role to remove with id '" + RoleID + "'" });
                }
            } 
            catch (Exception ex)
            {
                return BadRequest(new { status = "There was an exception: " + ex });
            }
        }
    }
}
