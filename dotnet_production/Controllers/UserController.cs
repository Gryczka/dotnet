using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using dotnet_production.Models.Entity;
using dotnet_production.Models.Object;
using dotnet_production.Controllers.Helpers;
using System.Text.Json;

namespace dotnet_production.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly dotnet_prodContext _context;
        private Helper _helper;

        public UserController(dotnet_prodContext context)
        {
            _context = context;
            _helper = new Helper(context);
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            try
            {
                List<UserTab> users = await _context.UserTab.ToListAsync();

                var listOfUsers = users.Select(user => {
                    List<string> roles = _helper.listOfNameRolesForUserId(user.UserId);
                    return new UserToReturn {
                        Id = user.UserId,
                        Name = user.Name,
                        Roles = roles
                    };
                }).ToList();
                if (listOfUsers == null) {
                    return NotFound(new { status = "Users do not exist" });
                } else {
                    return Ok(listOfUsers);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new { status = "There was an exception: " + ex });
            }
        }

        [HttpGet("{UserID}")]
        public async Task<IActionResult> GetUser(Guid UserID)
        {
            try
            {
                UserTab user = await _context.UserTab.SingleOrDefaultAsync(s => s.UserId == UserID);
                if (user == null) {
                    return NotFound(new { status = "There is no user to return with id '" + UserID + "'" });
                }
                List<string> roles = _helper.listOfNameRolesForUserId(user.UserId);
                return Ok(new UserToReturn{
                    Id = user.UserId,
                    Name = user.Name,
                    Roles = roles
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new { status = "There was an exception: " + ex });
            }
        }

        [HttpPost]
        public async Task<IActionResult> PostUser([FromBody] UserObj user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (user.Name == null) {
                        return BadRequest(new { status = "User name has to exist in json" });
                    }
                    if (user.RoleIds == null) {
                        return BadRequest(new { status = "User role have to exist in json" });
                    }
                    Guid userId = Guid.NewGuid();
                    var result = await _helper.collectUserRolesForRoleIdsFromUser(user, userId);
                    if (result._result == Result.NotFound) {
                        return NotFound( new { status = result._message});
                    }
                    List<UserRole> userRoles = result._list;

                    _context.UserTab.Add(new UserTab(user, userId, userRoles));
                    await _context.SaveChangesAsync();
                    return Ok(new GuidToReturn{ Id = userId });
                }
                else
                {
                    return BadRequest(new { status = "Model is invalid" });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new { status = "There was an exception: " + ex });
            }
        }

        [HttpPut("{UserID}")]
        public async Task<IActionResult> PutUser([FromRoute] Guid UserID, [FromBody] UserObj user) {
            try
            {
                if (ModelState.IsValid)
                {
                    if (user.Name == null) {
                        return BadRequest(new { status = "User name has to exist in json" });
                    }
                    if (user.RoleIds == null) {
                        return BadRequest(new { status = "User role ids have to exist in json" });
                    }
                    UserTab userToRemove = await _context.UserTab.SingleOrDefaultAsync(s => s.UserId == UserID);
                    if (userToRemove == null) return NotFound (new { status = "Not found user with id '" + UserID + "'" });
                    await _helper.removesUserRolesWithUserId(UserID);
                    var result = await _helper.collectUserRolesForRoleIdsFromUser(user, UserID);
                    if (result._result == Result.NotFound) {
                        return NotFound( new { status = result._message});
                    }
                    List<UserRole> userRolesToAdd = result._list;
                    if (userToRemove != null)  {
                        _context.Remove(userToRemove);
                    }

                    _context.UserTab.Add(new UserTab(user, UserID, userRolesToAdd));
                    await _context.SaveChangesAsync();
                    return Ok(new GuidToReturn{ Id = UserID});
                }
                else
                {
                    return BadRequest(new { status = "Model is invalid" });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new { status = "There was an exception: " + ex });
            }
        }

        [HttpDelete("{UserID}")]
        public async Task<IActionResult> DeleteUser(Guid UserID)
        {
            try
            {
                UserTab userToRemove = await _context.UserTab.SingleOrDefaultAsync(u => u.UserId == UserID);
                await _helper.removesUserRolesWithUserId(UserID);
                if (userToRemove != null)  {
                    _context.Remove(userToRemove);
                    await _context.SaveChangesAsync();
                    return Ok(new GuidToReturn{ Id = UserID});
                } else {
                    return NotFound(new { status = "There is no user to remove with id '" + UserID + "'"});
                }
            } 
            catch (Exception ex)
            {
                return BadRequest(new { status = "There was an exception: " + ex });
            }
        }
    }
}
