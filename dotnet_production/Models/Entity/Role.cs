﻿using System;
using System.Collections.Generic;
using dotnet_production.Models.Object;

namespace dotnet_production.Models.Entity
{
    public partial class Role
    {
        public Role()
        {
            UserRole = new HashSet<UserRole>();
        }

        public Guid RoleId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<UserRole> UserRole { get; set; }
        public Role(RoleObj user, Guid userId, List<UserRole> userRoles) {
            RoleId = userId;
            Name = user.Name;
            UserRole = userRoles;
        }
    }
}
