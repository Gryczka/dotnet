﻿using System;
using System.Collections.Generic;

namespace dotnet_production.Models.Entity
{
    public partial class UserRole
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }

        public virtual Role Role { get; set; }
        public virtual UserTab User { get; set; }
    }
}
