﻿using System;
using System.Collections.Generic;
using dotnet_production.Models.Object;

namespace dotnet_production.Models.Entity
{
    public partial class UserTab
    {
        public UserTab()
        {
            UserRole = new HashSet<UserRole>();
        }

        public Guid UserId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<UserRole> UserRole { get; set; }

        public UserTab(UserObj user, Guid userId, List<UserRole> userRoles) {
            UserId = userId;
            Name = user.Name;
            UserRole = userRoles;
        }
    }
}
