using System;
using System.Collections.Generic;

namespace dotnet_production.Models.Object
{
    public partial class GuidToReturn
    {
        public Guid Id { get; set; }
    }
}
