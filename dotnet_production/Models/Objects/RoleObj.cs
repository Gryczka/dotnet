using System;
using System.Collections.Generic;

namespace dotnet_production.Models.Object
{
    public partial class RoleObj
    {
        public string Name { get; set; }
        public List<Guid> UserIds { get; set; }
    }
}
