using System;
using System.Collections.Generic;

namespace dotnet_production.Models.Object
{
    public partial class RoleToReturn
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<string> Users { get; set; }
    }
}
