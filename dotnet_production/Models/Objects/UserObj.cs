using System;
using System.Collections.Generic;

namespace dotnet_production.Models.Object
{
    public partial class UserObj
    {
        public string Name { get; set; }
        public List<Guid> RoleIds { get; set; }
    }
}
