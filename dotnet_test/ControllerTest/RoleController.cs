using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using dotnet_production.Models.Entity;
using dotnet_production.Controllers;
using System.Text.Json;
using Moq;
using Microsoft.Extensions.Logging;
using dotnet_production.Models.Object;
using dotnet_test.Infrastructure;

namespace dotnet_test.ControllerTest
{
    [TestFixture]
    public class RoleControllerTests
    {
        List<Guid> roleIds = new List<Guid>();

        [OneTimeSetUp]
        public void Setup()
        {
            var context = new dotnet_testContext();
            var firstId = Guid.NewGuid();
            context.Role.Add(new Role { RoleId = firstId, Name = "Developer", UserRole = new List<UserRole>() });
            var secondId = Guid.NewGuid();
            context.Role.Add(new Role { RoleId = secondId, Name = "Tester", UserRole = new List<UserRole>() });
            roleIds.Add(firstId);
            roleIds.Add(secondId);
            context.SaveChanges();
        }

        [OneTimeTearDown]
        public void TearDown() {
            var context = new dotnet_testContext();
            var rolesToRemove = context.Role.Where(r => roleIds.Contains(r.RoleId));
            context.RemoveRange(rolesToRemove);
            context.SaveChanges();
        }

        [Test]
        public async System.Threading.Tasks.Task getAllItems()
        {
            var context = new dotnet_testContext();
            var controller = new RoleController(context);

            IActionResult rawResponse = await controller.GetRoles();
            Assert.NotNull(rawResponse);
            Assert.IsInstanceOf<OkObjectResult>(rawResponse);
            var result = rawResponse as OkObjectResult;
            List<RoleToReturn> roles = (List<RoleToReturn>)result.Value;
            
            Assert.AreEqual(context.Role.ToList().Count(), roles.Count());
        }

        [Test]
        public async System.Threading.Tasks.Task getOneItem()
        {
            var context = new dotnet_testContext();
            var controller = new RoleController(context);

            IActionResult rawResponse = await controller.GetRole(roleIds[0]);
            Assert.NotNull(rawResponse);
            Assert.IsInstanceOf<OkObjectResult>(rawResponse);
            var result = rawResponse as OkObjectResult;
            RoleToReturn role = (RoleToReturn)result.Value;
            
            Assert.AreEqual("Developer", role.Name);
            Assert.AreEqual(roleIds[0], role.Id);
        }

        [Test]
        public async System.Threading.Tasks.Task insertOneRole()
        {
            var roleName = "Seller";
            var roleToPost = new RoleObj { Name = roleName, UserIds = new List<Guid>() };
            var context = new dotnet_testContext();
            var controller = new RoleController(context);

            Assert.IsEmpty(context.Role.Where(u => u.Name == roleName));
            
            IActionResult rawResponse = await controller.PostRole(roleToPost);
            Assert.NotNull(rawResponse);
            Assert.IsInstanceOf<OkObjectResult>(rawResponse);
            var result = rawResponse as OkObjectResult;
            GuidToReturn guid = (GuidToReturn)result.Value;
            
            roleIds.Add(guid.Id);
            var roleInDb = context.Role.Single(r => r.Name == roleName);
            Assert.AreEqual(roleInDb.Name, roleToPost.Name);
        }

        [Test]
        public async System.Threading.Tasks.Task updateOneRole()
        {
            var roleName = "Driver";
            var roleToPut = new RoleObj { Name = roleName, UserIds = new List<Guid>() };
            var context = new dotnet_testContext();
            var controller = new RoleController(context);

            IActionResult rawResponse = await controller.PutRole(roleIds[0], roleToPut);
            Assert.NotNull(rawResponse);
            Assert.IsInstanceOf<OkObjectResult>(rawResponse);
            var result = rawResponse as OkObjectResult;
            GuidToReturn guid = (GuidToReturn)result.Value;
            
            var roleInDb = context.Role.Single(r => r.RoleId == guid.Id);
            Assert.AreEqual(roleInDb.Name, roleToPut.Name);
            Assert.AreEqual(roleInDb.RoleId, roleIds[0]);
        }

        [Test]
        public async System.Threading.Tasks.Task deleteOneRole()
        {
            var context = new dotnet_testContext();
            var name = "Teacher";
            var id = Guid.NewGuid();
            context.Role.Add(new Role { RoleId = id, Name = name, UserRole = new List<UserRole>() });
            context.SaveChanges();
            var controller = new RoleController(context);
            
            var roleInDb = context.Role.Single(r => r.RoleId == id);
            Assert.AreEqual(roleInDb.Name, name);
            Assert.AreEqual(roleInDb.RoleId, id);

            IActionResult rawResponse = await controller.DeleteRole(id);
            Assert.NotNull(rawResponse);
            Assert.IsInstanceOf<OkObjectResult>(rawResponse);
            var result = rawResponse as OkObjectResult;
            GuidToReturn guid = (GuidToReturn)result.Value;

            Assert.IsEmpty(context.Role.Where(r => r.RoleId == guid.Id));
        }
    }
}