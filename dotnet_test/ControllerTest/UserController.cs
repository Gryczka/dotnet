using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using dotnet_production.Models.Entity;
using dotnet_production.Controllers;
using System.Text.Json;
using Moq;
using Microsoft.Extensions.Logging;
using dotnet_production.Models.Object;
using dotnet_test.Infrastructure;

namespace dotnet_test.ControllerTest
{
    [TestFixture]
    public class UserControllerTests
    {
        List<Guid> userIds = new List<Guid>();

        [OneTimeSetUp]
        public void Setup()
        {
            var context = new dotnet_testContext();
            var firstId = Guid.NewGuid();
            context.UserTab.Add(new UserTab { UserId = firstId, Name = "Magda", UserRole = new List<UserRole>() });
            var secondId = Guid.NewGuid();
            context.UserTab.Add(new UserTab { UserId = secondId, Name = "Benny", UserRole = new List<UserRole>() });
            userIds.Add(firstId);
            userIds.Add(secondId);
            context.SaveChanges();
        }

        [OneTimeTearDown]
        public void TearDown() {
            var context = new dotnet_testContext();
            var usersToRemove = context.UserTab.Where(u => userIds.Contains(u.UserId));
            context.RemoveRange(usersToRemove);
            context.SaveChanges();
        }

        [Test]
        public async System.Threading.Tasks.Task getAllItems()
        {
            var context = new dotnet_testContext();
            var controller = new UserController(context);

            IActionResult rawResponse = await controller.GetUsers();
            Assert.NotNull(rawResponse);
            Assert.IsInstanceOf<OkObjectResult>(rawResponse);
            var result = rawResponse as OkObjectResult;
            List<UserToReturn> users = (List<UserToReturn>)result.Value;
            
            Assert.AreEqual(context.UserTab.ToList().Count(), users.Count());
        }

        [Test]
        public async System.Threading.Tasks.Task getOneItem()
        {
            var context = new dotnet_testContext();
            var controller = new UserController(context);

            IActionResult rawResponse = await controller.GetUser(userIds[0]);
            Assert.NotNull(rawResponse);
            Assert.IsInstanceOf<OkObjectResult>(rawResponse);
            var result = rawResponse as OkObjectResult;
            UserToReturn user = (UserToReturn)result.Value;
            
            Assert.AreEqual("Magda", user.Name);
            Assert.AreEqual(userIds[0], user.Id);
        }

        [Test]
        public async System.Threading.Tasks.Task insertOneUser()
        {
            var userName = "Amadeusz";
            var userToPost = new UserObj { Name = userName, RoleIds = new List<Guid>() };
            var context = new dotnet_testContext();
            var controller = new UserController(context);

            Assert.IsEmpty(context.UserTab.Where(u => u.Name == userName));
            
            IActionResult rawResponse = await controller.PostUser(userToPost);
            Assert.NotNull(rawResponse);
            Assert.IsInstanceOf<OkObjectResult>(rawResponse);
            var result = rawResponse as OkObjectResult;
            GuidToReturn guid = (GuidToReturn)result.Value;
            
            userIds.Add(guid.Id);
            var userInDb = context.UserTab.Single(u => u.Name == userName);
            Assert.AreEqual(userInDb.Name, userToPost.Name);
        }

        [Test]
        public async System.Threading.Tasks.Task updateOneUser()
        {
            var userName = "Bartosz";
            var userToPut = new UserObj { Name = userName, RoleIds = new List<Guid>() };
            var context = new dotnet_testContext();
            var controller = new UserController(context);

            IActionResult rawResponse = await controller.PutUser(userIds[0], userToPut);
            Assert.NotNull(rawResponse);
            Assert.IsInstanceOf<OkObjectResult>(rawResponse);
            var result = rawResponse as OkObjectResult;
            GuidToReturn guid = (GuidToReturn)result.Value;
            
            var userInDb = context.UserTab.Single(u => u.UserId == guid.Id);
            Assert.AreEqual(userInDb.Name, userToPut.Name);
            Assert.AreEqual(userInDb.UserId, userIds[0]);
        }

        [Test]
        public async System.Threading.Tasks.Task deleteOneUser()
        {
            var context = new dotnet_testContext();
            var name = "Tomasz";
            var id = Guid.NewGuid();
            context.UserTab.Add(new UserTab { UserId = id, Name = name, UserRole = new List<UserRole>() });
            context.SaveChanges();
            var controller = new UserController(context);
            
            var userInDb = context.UserTab.Single(u => u.UserId == id);
            Assert.AreEqual(userInDb.Name, name);
            Assert.AreEqual(userInDb.UserId, id);

            IActionResult rawResponse = await controller.DeleteUser(id);
            Assert.NotNull(rawResponse);
            Assert.IsInstanceOf<OkObjectResult>(rawResponse);
            var result = rawResponse as OkObjectResult;
            GuidToReturn guid = (GuidToReturn)result.Value;

            Assert.IsEmpty(context.Role.Where(r => r.RoleId == guid.Id));
        }
    }
}