using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Collections.Generic;
using dotnet_production.Models.Entity;

namespace dotnet_test.Infrastructure
{
    public class dotnet_testContext : dotnet_prodContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Host=localhost;Port=5433;Username=test;Password=test;Database=dotnet_test;");
            }
        }
    }
}
